package com.danielhutchinson.connectfour.ai.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.danielhutchinson.connectfour.ai.ConnectFour;
import com.danielhutchinson.connectfour.ai.utils.Assets;
import com.danielhutchinson.connectfour.ai.utils.Constants;


/**
 * Created by Daniel on 14/12/2015.
 *
 * Creates the main menu screen. Its changes the difficulty of the game
 * by pressing the difficulty button.
 *
 * It also leads to the settings page by clicking the settings button and
 * to the game screen by pressing the play button.
 */
public class MenuScreen implements Screen{

    ConnectFour game;
    Stage stage;
    TextButton playBtn, settingsBtn, difficultyBtn;
    Table buttonsTable;
    final int NUM_BUTTONS = 3;

    OrthographicCamera camera;

    Image backgroundImg;

    public MenuScreen(ConnectFour game) {
        this.game = game;
    }

    @Override
    public void show() {
        stage = new Stage();
        camera = new OrthographicCamera(Constants.APP_WIDTH, Constants.APP_HEIGHT);
        camera.translate(Constants.APP_WIDTH / 2, Constants.APP_HEIGHT / 2);

        backgroundImg = new Image(new Texture("img/background/background1.jpg"));
        backgroundImg.setSize(Constants.APP_WIDTH, Constants.APP_HEIGHT);
        stage.addActor(backgroundImg);

        // set up buttons using skin
        playBtn       = new TextButton("Play",       Assets.skin);
        settingsBtn   = new TextButton("Settings",   Assets.skin);
        difficultyBtn = new TextButton("Difficulty: \n" + game.getDifficulty().name().toLowerCase(),
                                                                                        Assets.skin);

        buttonsTable = new Table(Assets.skin);
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(0.5f, 0.5f, 0.5f, 0.6f);
        buttonsTable.setBackground(new NinePatchDrawable(new NinePatch(new Texture(pixmap))));

        // default values for anything added to the buttons table
        buttonsTable.defaults().size(Gdx.graphics.getWidth() / 4f, Gdx.graphics.getHeight()
                / (NUM_BUTTONS * 2)).pad(20);

        buttonsTable.add(playBtn).row();
        buttonsTable.add(settingsBtn).row();
        buttonsTable.add(difficultyBtn);

        buttonsTable.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);

        stage.addActor(buttonsTable);

        setupListeners();

        Gdx.input.setInputProcessor( stage );
    }

    @Override
    public void render(float delta) {
        //Background colour
        Gdx.gl.glClearColor(0.9f, 0.9f, 0.9f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private void setupListeners() {
        playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                //Open the game screen
                game.setScreen( new GameScreen(game) );
            }
        });
        settingsBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                // allow to select if it uses the optimizing algorithm
                // set pref colours of the chip
            }
        });
        difficultyBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                //Change the difficulty on click
                game.nextDifficulty();
                difficultyBtn.setText( "Difficulty: \n" + game.getDifficulty().name().toLowerCase() );
            }
        });
    }

    public void findDifficulty() {

    }

}// End class
