package com.danielhutchinson.connectfour.ai.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.danielhutchinson.connectfour.ai.ConnectFour;
import com.danielhutchinson.connectfour.ai.board.Board;
import com.danielhutchinson.connectfour.ai.enums.Difficulty;
import com.danielhutchinson.connectfour.ai.mechanics.agent.AgentBehaviour;
import com.danielhutchinson.connectfour.ai.utils.Assets;
import com.danielhutchinson.connectfour.ai.utils.Constants;
import com.danielhutchinson.connectfour.ai.utils.Utils;

/**
 * Created by Daniel on 14/12/2015.
 *
 * The screen which displays all of the game components. It draws the board,
 * the menu and sets the size of the widgets inside it. It also holds important
 * player move methods.
 */
public class GameScreen implements Screen {

    /*
     *************************************************************************************
     *                                     ATTRIBUTES                                    *
     *************************************************************************************/

    /* libGDX attributes */
    OrthographicCamera camera;
    ConnectFour game;
    Stage stage;

    Image boardImage, backgroundImg;
    Table menu;

    /* game attributes */
    Board board;

    boolean isPlayerWinner = false; //Player info
    boolean playersTurn = true;
    boolean won  = false;
    boolean draw = false;

    AgentBehaviour agentBehaviour;

    Difficulty difficulty;



    /*
     ************************************************************************************
     *                                   CONSTRUCTORS                                   *
     ************************************************************************************/

    public GameScreen(ConnectFour game) {
        this.game = game;
    }

    public GameScreen(){
    }


    /*
     *************************************************************************************
     *                                 ESSENTIAL METHODS                                 *
     *************************************************************************************/
    /* essential for libGDX & gui */
    @Override
    public void show() {

        stage = new Stage();
        camera = new OrthographicCamera(Constants.APP_WIDTH, Constants.APP_HEIGHT);
        camera.translate(Constants.APP_WIDTH / 2, Constants.APP_HEIGHT / 2);
        stage.getViewport().setCamera(camera);
        backgroundImg = new Image(new Texture("img/background/background1.jpg"));
        backgroundImg.setSize(Constants.APP_WIDTH, Constants.APP_HEIGHT);
        stage.addActor(backgroundImg);

        board = new Board(this);
        setMenu();

        int menuBoardGap = 100;
        board.setPosition((Constants.APP_WIDTH - (menuBoardGap + Constants.BOARD_WIDTH + Constants.MENU_WIDTH)) / 2,
                (Constants.APP_HEIGHT / 2) - (Constants.BOARD_HEIGHT / 2));
        menu.setPosition(menuBoardGap + board.getX() + board.getWidth(), board.getY());
        stage.addActor(board);
        stage.addActor(menu);


        boardImage = new Image(new Texture("img/boardImage.png"));
        boardImage.setSize(board.getWidth(), board.getHeight());
        boardImage.setPosition(board.getX(), board.getY());

        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.9f, 0.9f, 0.9f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
        stage.getBatch().begin();
        boardImage.draw(stage.getBatch(), 1);
        stage.getBatch().end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }


    /*
     ************************************************************************************
     *                                        METHODS                                   *
     ************************************************************************************/

    //-------------------------------------------------------- SETUP METHODS

    private TextButton hintBtn, resetBtn, homeBtn;
    private Label turnLbl;

    private void setMenu() {
        //Create the root menu table
        menu = new Table();
        menu.setSize(Constants.MENU_WIDTH, Constants.MENU_HEIGHT);

        //Set the labels
        Label difficultyLbl = new Label("Difficulty:\n" + game.getDifficulty().name().toLowerCase(), Assets.skin);
        turnLbl = new Label("Turn:\n" + (playersTurn ? "player" : "AI"), Assets.skin);

        //create buttons for menu
        homeBtn  = new TextButton("Home",  Assets.skin);
        resetBtn = new TextButton("Reset", Assets.skin);
        hintBtn  = new TextButton("Hint",  Assets.skin);

        //Add everything
        menu.pad(20);
        menu.add(difficultyLbl).pad(10).left().row();
        menu.add(turnLbl).pad(10).expand().left().top().row();

        menu.add(hintBtn).pad(10).row();
        menu.add(resetBtn).pad(10).row();
        menu.add(homeBtn).pad(10);

        setUpListeners();
    }


    //----------------------------------------------------------- TURN METHODS & TIMERS
    //After players turn, take AI turn & stop input
    public void takePlayersTurn(int column) {
        board.placeCounter(column);

        //Stop user input
        Gdx.input.setInputProcessor(null);
        setPlayersTurn(false);

        //Timer needed to slow down play.
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                takeAITurn();
            }
        }, 0.7f);
    }

    public void takeAITurn() {
        agentBehaviour = new AgentBehaviour(board, game, this);
        agentBehaviour.takeTurn();

        setPlayersTurn(true);
        Gdx.input.setInputProcessor(stage);

        if(won) {
            board.resetBoard();
            won = false; //replay
        }
    }

    private void stopUserInput() {
        Gdx.input.setInputProcessor(null);
        setPlayersTurn(false);
    }


    //------------------------------------------------------- BUTTON METHODS
    private void setUpListeners() {

        hintBtn.addListener(new ClickListener() {
            @Override
            public void clicked(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                //Open the game screen
                game.setScreen(new GameScreen(game));
            }
        });

        resetBtn.addListener(new ClickListener() {
            @Override
            public void clicked(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                board.resetBoard();
                //setPlayersTurn(true);
            }
        });

        homeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                //Open the game screen
                game.setScreen( new MenuScreen(game) );
            }
        });

    }

    /*
     ***************************************************************************************
     *                                 GETTERS & SETTERS                                   *
     ***************************************************************************************
     */

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public boolean isPlayerWinner() {
        return isPlayerWinner;
    }

    public void setIsPlayerWinner(boolean isPlayerWinner) {
        this.isPlayerWinner = isPlayerWinner;
    }

    public boolean isPlayersTurn() {
        return playersTurn;
    }

    public void setPlayersTurn(boolean playersTurn) {
        this.playersTurn = playersTurn;
        turnLbl.setText("Turn:\n" + (playersTurn ? "Player" : "AI"));
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public void setRandomTurn() {
        //50-50 chance of either getting chose first.
        int randomNo = Utils.randomValueBetween(11, 0);
        if (randomNo > 6) setPlayersTurn(true);
        else setPlayersTurn(false);
    }

    public void setDifficulty() {
        this.difficulty = game.getDifficulty();
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

}// End class
