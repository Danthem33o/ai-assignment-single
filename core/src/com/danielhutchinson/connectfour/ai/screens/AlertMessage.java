package com.danielhutchinson.connectfour.ai.screens;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.danielhutchinson.connectfour.ai.utils.Assets;

/**
 * Created by Daniel on 17/12/2015.
 *
 * Simple alert box. Shows when the user has won/lost the game.
 */
public class AlertMessage extends Table {

    //TODO

    Label text;
    TextButton okBtn;
    GameScreen gameScreen;

    private static final int HEIGHT = 500, WIDTH = 500;

    public AlertMessage(String message) {
        text = new Label(message, Assets.skin);
        setSize(WIDTH, HEIGHT);

        create();
    }

    //Constructor that sets the width + size also


    private void create() {
        //Set the image
        okBtn = new TextButton("Ok", Assets.skin);

    }

}//End class
