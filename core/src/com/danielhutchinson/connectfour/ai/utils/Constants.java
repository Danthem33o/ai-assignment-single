package com.danielhutchinson.connectfour.ai.utils;

/**
 * Created by Daniel on 16/12/2015.
 *
 * Constants class that holds all of the constant values
 * that are used throughout the program.
 */
public class Constants {

    public static final int APP_WIDTH = 1920, APP_HEIGHT = 1080;
    // board width no more than 1260
    public static final int BOARD_WIDTH = 1000, BOARD_HEIGHT = 6 * BOARD_WIDTH / 7;
    public static final int MENU_WIDTH = 400, MENU_HEIGHT = BOARD_HEIGHT;
}
