package com.danielhutchinson.connectfour.ai.utils;


import java.util.Random;

/**
 * Created by Daniel on 20/12/2015.
 *
 * Utils class tha holds all of the utility methods used
 * throughout the program.
 */
public class Utils {

    /**
     * Returns a random number between the values of max
     * and minimum that is passed through the parameters.
     *
     * @param max the max the random number can be
     * @param min the min the random number can be
     * @return int a random integer number between max & min
     */
    public static int randomValueBetween(int max, int min) {
        Random random = new Random();
        return random.nextInt((max - min) +1) + min;
    }

}// End class
