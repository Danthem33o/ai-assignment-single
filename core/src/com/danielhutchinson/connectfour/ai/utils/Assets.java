package com.danielhutchinson.connectfour.ai.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Daniel on 14/12/2015.
 *
 * Handles the asset skins used by the libGDX widgets.
 */
public class Assets {

    public static AssetManager manager;
    public static Skin skin;

    public static void create() {

        manager = new AssetManager();


        // adding ttf to skin.
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Oxygen-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 36;
        BitmapFont font24 = generator.generateFont(parameter); // font size 30 pixels
        generator.dispose(); // don't forget to dispose to avoid memory leaks!

        // loading the skin. Skin does not need to be disposed as used throughout.
        skin = new Skin();
        skin.add("font24",font24);

        skin.addRegions(new TextureAtlas(Gdx.files.internal("data/uiskin.atlas")));
        skin.load(Gdx.files.internal("data/uiskin.json"));

    }

}//End class
