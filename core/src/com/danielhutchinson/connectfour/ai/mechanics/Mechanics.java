package com.danielhutchinson.connectfour.ai.mechanics;


import com.danielhutchinson.connectfour.ai.board.Board;

/**
 * Created by Daniel on 18/12/2015.
 *
 * There are over two trillion1, 904, 587, 136, 600 ways to achieve connect
 * four on the board.
 * More than four trillion (4,531, 985, 219, 092 )unique ways to fill the
 * standard board.
 *
 * A perfect player knows that if a player has the first move and places
 * their chip in the middle, then they know they will lose withing 41 moves.
 */
public class Mechanics {

    //Arrays
    private int[] scores, moves;

    private static final int WIN_MOVE  =   1;
    private static final int LOSE_MOVE =  -1;
    private static final int DRAW_MOVE =   0;

    private static final int WIN_GAME  =  10;
    private static final int LOSE_GAME = -10;

    Board board;

    public Mechanics(Board board) {
        this.board = board;
    }

    public void minimax() {

        //if (game.win) -player return 10
        //else if (game.win) return -10;
        //else return 0;

       // for (int column = 0; column < ) //TODO - add the Minimax algorithm


    }

    /**
     * Find the best move to make.
     *
     * @return The best column (move) to make.
     */
    public void findBestMove() {


    }


    public int[] getScores() {
        return scores;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    public int[] getMoves() {
        return moves;
    }

    public void setMoves(int[] moves) {
        this.moves = moves;
    }
}// End class
