package com.danielhutchinson.connectfour.ai.mechanics.agent;

import com.danielhutchinson.connectfour.ai.ConnectFour;
import com.danielhutchinson.connectfour.ai.board.Board;
import com.danielhutchinson.connectfour.ai.mechanics.ai.game_tree.MiniMax;
import com.danielhutchinson.connectfour.ai.screens.GameScreen;
import com.danielhutchinson.connectfour.ai.utils.Utils;


/**
 * Created by Daniel on 18/12/2015.
 *
 * This is what gives the agent it's 'human' behaviour.
 *
 */
public class AgentBehaviour {

    /*
     *********************************************************************************
     *                                     VARIABLES                                 *
     *********************************************************************************/

    private static int       CHANCE;             //% values
    private static final int MINIMUM = 0, MAX = 100;

    private GameScreen gameScreen;
    private ConnectFour game;
    private Board board;



    /*
     *********************************************************************************
     *                                    CONSTRUCTORS                               *
     *********************************************************************************/
    public AgentBehaviour(Board board, ConnectFour connectFour) {
        this.board = board;
        game = connectFour;

        setAgentDifficulty();
    }

    public AgentBehaviour(Board board, ConnectFour connectFour, GameScreen gameScreen) {
        this.board = board;
        game = connectFour;
        this.gameScreen = gameScreen;

        setAgentDifficulty();
    }


    /*
     *********************************************************************************
     *                                      METHODS                                  *
     *********************************************************************************/

    //---------------------------------------------------- TURN METHODS

    /**
     * takeTurn()
     *
     * Takes the AI agent's turn and places the counter on the board.
     *
     * Checks if the move is successful depending on the difficulty by
     * calling isMoveSuccessful().
     *
     * If move is successful, it places the counter on the board in the
     * best column, which is found by calling getBestMove().
     * If not, then the counter is placed in a random location on the
     * board by calling makeRandomMove().
     */
    public void takeTurn() {
        int column;

        if (isMoveSuccessful()) {
            MiniMax miniMax = new MiniMax(board, gameScreen);
            //column = miniMax.makeMove();
            column = getBestMove();

        } else  {
            column = board.makeRandomMove();
        }

        printDifficulty();
        board.placeCounter(column);
    }

    /**
     * for testing purposes
     * TODO: REMOVE
     * @return column
     */
    public int getBestMove() {
        //Evaluate board //TODO
        return board.makeRandomMove();
    }

    /**
     * Checks if the move can be made. This is what gives
     * the AI agent it's difficulty behaviour.
     *
     * It receives a random value from randomValueBetween()
     * that is in the Utils class, which is stored in r.
     *
     * The random value is then checked against CHANCE.
     * If r is less than CHANCE, then the move is made
     * and returns true, otherwise it returns false.
     *
     * CHANCE is set in setAgentDifficulty().
     *
     * @return boolean  return true or false
     */
    public boolean isMoveSuccessful() {
        int r = Utils.randomValueBetween(MAX, MINIMUM);
        return r <= CHANCE;
    }



    /*
     ***********************************************************************************
     *                                  GETTERS & SETTERS                              *
     ***********************************************************************************/

    public int getCHANCE() {
        return CHANCE;
    }

    public void setCHANCE(int CHANCE) {
        this.CHANCE = CHANCE;
    }

    /**
     * Difficulty of the program is set depending on the difficulty set
     */
    public void setAgentDifficulty() {
        String difficulty =  game.getDifficulty().name();

        if (difficulty.equals("EASY"))             CHANCE = 25; //of making correct move...
        else if (difficulty.equals("MEDIUM"))      CHANCE = 50;
        else if (difficulty.equals("HARD"))        CHANCE = 75;
        else if (difficulty.equals("EXTREME"))     CHANCE = 90;
        else if (difficulty.equals("IMPOSSIBLE"))  CHANCE = 100;

    }

    //TODO remove
    public void printDifficulty() {
        String difficulty =  game.getDifficulty().name();

        System.out.println("difficulty: " + difficulty);
    }


}//End class
