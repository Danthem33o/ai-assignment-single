package com.danielhutchinson.connectfour.ai.mechanics.ai.game_tree;

/**
 * Created by Daniel on 02/01/2016.
 */
public class Analysis {

    int whoWon = 0;

    /*
        // Horizontal one moves Player 1
        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 1 && board[row][col + 1] == 0 && board[row][col + 2] == 0 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 125;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 1 && board[row][col + 2] == 0 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 125;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 0 && board[row][col + 2] == 1 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 125;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 0 && board[row][col + 2] == 0 && board[row][col + 3] == 1) {
                    whoWon = whoWon - 125;
                }
            }
        }
        //Horizontal One Moves End Player One

        // Horizontal one moves Player Two
        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 2 && board[row][col + 1] == 0 && board[row][col + 2] == 0 && board[row][col + 3] == 0) {
                    whoWon = whoWon + 125;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 2 && board[row][col + 2] == 0 && board[row][col + 3] == 0) {
                    whoWon = whoWon + 125;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 0 && board[row][col + 2] == 2 && board[row][col + 3] == 0) {
                    whoWon = whoWon + 125;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 0 && board[row][col + 2] == 0 && board[row][col + 3] == 2) {
                    whoWon = whoWon + 125;
                }
            }
        }
        //Horizontal One Moves End Player Two

        //Horizontal Two Moves Player One
        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 1 && board[row][col + 1] == 1 && board[row][col + 2] == 0 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 1 && board[row][col + 2] == 1 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 0 && board[row][col + 2] == 1 && board[row][col + 3] == 1) {
                    whoWon = whoWon - 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 1 && board[row][col + 1] == 0 && board[row][col + 2] == 1 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 1 && board[row][col + 2] == 0 && board[row][col + 3] == 1) {
                    whoWon = whoWon - 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 1 && board[row][col + 1] == 0 && board[row][col + 2] == 0 && board[row][col + 3] == 1) {
                    whoWon = whoWon - 250;
                }
            }
        }

        //Horizontal Two Moves Player One End

        //Horizontal Two Moves Player Two
        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 1 && board[row][col + 1] == 1 && board[row][col + 2] == 0 && board[row][col + 3] == 0) {
                    whoWon = whoWon - 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 2 && board[row][col + 2] == 2 && board[row][col + 3] == 0) {
                    whoWon = whoWon + 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 0 && board[row][col + 2] == 2 && board[row][col + 3] == 2) {
                    whoWon = whoWon + 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 2 && board[row][col + 1] == 0 && board[row][col + 2] == 2 && board[row][col + 3] == 0) {
                    whoWon = whoWon + 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 0 && board[row][col + 1] == 2 && board[row][col + 2] == 0 && board[row][col + 3] == 2) {
                    whoWon = whoWon + 250;
                }
            }
        }

        for (int col = 0; col <= 3; col++) {
            for (int row = 0; row < 6; row++) {
                if (board[row][col] == 2 && board[row][col + 1] == 0 && board[row][col + 2] == 0 && board[row][col + 3] == 2) {
                    whoWon = whoWon + 250;
                }
            }
        }
        //Horizontal Two Moves Player Two End
        if (difficulty == HARD) {
            //Horizontal Three Moves Player One

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 1 && board[row][col + 1] == 1 && board[row][col + 2] == 1 && board[row][col + 3] == 0) {
                        whoWon = whoWon - 1000;
                    }
                }
            }

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 0 && board[row][col + 1] == 1 && board[row][col + 2] == 1 && board[row][col + 3] == 1) {
                        whoWon = whoWon - 1000;
                    }
                }
            }

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 1 && board[row][col + 1] == 0 && board[row][col + 2] == 1 && board[row][col + 3] == 1) {
                        whoWon = whoWon - 1000;
                    }
                }
            }

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 1 && board[row][col + 1] == 1 && board[row][col + 2] == 0 && board[row][col + 3] == 1) {
                        whoWon = whoWon - 1000;
                    }
                }
            }
            //Horizontal Three Moves Player One End

            //Horizontal Three Moves Player Two

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 2 && board[row][col + 1] == 2 && board[row][col + 2] == 2 && board[row][col + 3] == 0) {
                        whoWon = whoWon + 1000;
                    }
                }
            }

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 0 && board[row][col + 1] == 2 && board[row][col + 2] == 2 && board[row][col + 3] == 2) {
                        whoWon = whoWon + 1000;
                    }
                }
            }

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 2 && board[row][col + 1] == 0 && board[row][col + 2] == 2 && board[row][col + 3] == 2) {
                        whoWon = whoWon + 1000;
                    }
                }
            }

            for (int col = 0; col <= 3; col++) {
                for (int row = 0; row < 6; row++) {
                    if (board[row][col] == 2 && board[row][col + 1] == 2 && board[row][col + 2] == 0 && board[row][col + 3] == 2) {
                        whoWon = whoWon + 1000;
                    }
                }
            }
        }
        //Horizontal Three Moves Player Two End
    }
*/


}
