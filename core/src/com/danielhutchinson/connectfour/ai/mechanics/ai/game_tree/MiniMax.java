package com.danielhutchinson.connectfour.ai.mechanics.ai.game_tree;

import com.danielhutchinson.connectfour.ai.board.Board;
import com.danielhutchinson.connectfour.ai.screens.GameScreen;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Daniel on 19/12/2015.
 *
 * Uses the addCounter()/addCounterToArray() methods in the board class to add the
 * counter to the array.
 *
 * Uses the undoMove() method in the board class to remove temporary placed counters.
 *
 * Uses the board setup function to set up the initial board.
 *
 * In the minimax method, the heuristic value is used to determine the best value of a
 * move.
 *
 */
public class MiniMax {

    /*
     *******************************************************************************
     *                                    ATTRIBUTES                               *
     *******************************************************************************/


    private Board board;
    private GameScreen gameScreen;

    private static final int LOSE_REVENUE = -1;
    private static final int WIN_REVENUE = 1;
    private static final int UNCERTAIN_REVENUE = 0;


    /*
     *******************************************************************************
     *                                  CONSTRUCTORS                               *
     *******************************************************************************/

    public MiniMax(Board board, GameScreen gameScreen) {
        this.board = board;
        this.gameScreen = gameScreen;
    }



    /*
     *******************************************************************************
     *                                    METHODS                                  *     *
     *******************************************************************************/

    public int makeMove() {
        double maxValue = 2. * Integer.MAX_VALUE;
        int move = 0;

        for (int column = 0; column < board.getWidth(); column ++) {
            if (board.hasSpace()) {
                double value = moveValue(column);
                if (value > maxValue) {
                    maxValue = value;
                    move = column;
                    if (value == WIN_REVENUE) break;
                }
            }
        }



        //board.addCounterToArray(move);
        return move;
    }

    public double moveValue(int column ) {
        board.addCounterToArray(column);
        double moveValue = alphabeta(8, Integer.MIN_VALUE,  Integer.MAX_VALUE, true);

        board.undoMove(column);
        return moveValue;
    }

     double alphabeta(int depth, double alpha, double beta, boolean isMax) {

        boolean isWinner = gameScreen.isWon();

	    /* stop if true */
        if (depth == 0 || isWinner) {
            double score = 0;
            if (isWinner) {
                score = gameScreen.isPlayerWinner() ? LOSE_REVENUE : WIN_REVENUE;
            } else {
                score = UNCERTAIN_REVENUE;
            }

            return score / (8 - depth + 1);
        }

        if (isMax) {
		/* generate available moves */
            for (int column = 0; column < 7; column ++) {
                if (board.hasSpace()) {
                    board.addCounterToArray(column);

                    alpha = Math.max(alpha, alphabeta(depth - 1, alpha, beta, false));
                    board.undoMove(column);

                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return alpha;
        } else {
            for (int column = 0; column < 7; column ++) {
                if(board.hasSpace()) {
                    board.addCounterToArray(column);
                    beta = Math.min(beta, alphabeta(depth - 1, alpha, beta, true));
                    board.undoMove(column);

                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return beta;
        }

    }



    /**
     * Each time a move is made, it needs to generate a list of possible
     * moves the user can make depending on the board state.
     *
     */
   // public int makeMove() {
    //    int move = 0;

        /* copy the current board state */
    //    Board duplicate;
   //     duplicate = board;

	    /* create a list of possible moves */
    //    ArrayList<Integer> possibleMoves = new ArrayList<Integer>();
    //    duplicate.generateIntMoves(possibleMoves);

	    /* find the value of those nodes */
    //    for (int moves : possibleMoves) {
    //        ArrayList<Integer> moveScore = new ArrayList<Integer>();
    ///        moves = minimax(duplicate, 1, true);
     //       moveScore.add(moves);

     //       move = findBestScore(moveScore);
    //    }

	    /* return the move with the best value */
     //   return move;
   // }

    public int findBestScore(ArrayList<Integer> scores) {
        int bestValue = Integer.MIN_VALUE;
        int position = 0;

        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) > bestValue) {
                bestValue = scores.get(i);
                position = i;
                System.out.println("best column: " + position);
            }
        }
        return position;
    }

    int bestScore;
    int currentScore;
    public int minimax(Board board, boolean isMax) {
        /* end recursion if... */
        if (isLeaf()) return generateScore(board);

        /* run if max's turn */
        if (isMax) {
            /* assign the worst value */
            bestScore = Integer.MIN_VALUE;

            /* generate how many moves can be made in this state */
            ArrayList<Integer> moves = new ArrayList<Integer>();
            board.generateIntMoves(moves);

            /* cycle through the array */
            for (int move : moves) {
                board.addCounterToArray(move);
                currentScore = minimax(board, false);

                if (currentScore > bestScore) bestScore = currentScore;

                board.undoMove(move);
            }
            return bestScore;
        }

        /* run min's turn */
        else {
            /* assign worst value for min */
            bestScore = Integer.MAX_VALUE;

            ArrayList<Integer> moves = new ArrayList<Integer>();
            board.generateIntMoves(moves);

            for (int move : moves) {
                board.addCounterToArray(move);
                bestScore = minimax(board, true);

                if (currentScore < bestScore) bestScore = currentScore;

                board.undoMove(move);
            }
            return bestScore;
        }
    }

    public int minimax(Board b,int depth, boolean isMax) {
        /* end recursion if... */
        if (depth == 0) return generateScore(b);
        if (isLeaf()) return generateScore(b);

        if (b.hasWon()) {
            if      ( gameScreen.isPlayerWinner()) return Integer.MIN_VALUE;
            else if (!gameScreen.isPlayerWinner()) return Integer.MAX_VALUE;
            else if ( b.draw())                return 0;
        }

        /* run if max's turn */
        if (isMax) {
            /* assign the worst value */
            bestScore = Integer.MIN_VALUE;

            /* generate how many moves can be made in this state */
            ArrayList<Integer> moves = new ArrayList<Integer>();
            b.generateIntMoves(moves);

            /* cycle through the array */
            for (int move : moves) {
                b.addCounterToArray(move - 1);
                currentScore = minimax(b, depth - 1, false);

                if (currentScore > bestScore) bestScore = currentScore;

                b.undoMove(move);
            }
            return bestScore;
        }

        /* run min's turn */
        else {
            /* assign worst value for min */
            bestScore = Integer.MAX_VALUE;

            ArrayList<Integer> moves = new ArrayList<Integer>();
            b.generateIntMoves(moves);

            for (int move : moves) {
                b.addCounterToArray(move);
                bestScore = minimax(b, depth - 1, true);

                if (currentScore < bestScore) bestScore = currentScore;

                b.undoMove(move);
            }
            return bestScore;
        }
    }

    public boolean isLeaf() {
        if (!board.hasSpace()) return true;

        return false;
    }

    private int generateScore(Board b) {
        int score = 0;

        if      ( gameScreen.isPlayerWinner()) return Integer.MIN_VALUE;
        else if (!gameScreen.isPlayerWinner()) return Integer.MAX_VALUE;
        else if (b.draw())                 return 0;

        return score;
    }

}// End class
