package com.danielhutchinson.connectfour.ai.mechanics.ai.game_tree;

import com.danielhutchinson.connectfour.ai.board.Board;
import com.danielhutchinson.connectfour.ai.enums.NodeType;
import com.danielhutchinson.connectfour.ai.screens.GameScreen;

import java.util.ArrayList;

/**
 * Created by Daniel on 21/12/2015.
 *
 * Changes and gets the node type from the enum class.
 *
 * Stores a board state after a move is made. Each state
 * is a temporary board value given to the node.
 *
 * A node has an evaluation function included to find out the value
 * of it's board state.
 */
public class Node {

    /*
     ***********************************************************************
     *                              ATTRIBUTES                             *
     ***********************************************************************/

    private int    columnID;
    private int value;
    private int bestValue;

    private NodeType nodeType;
    private ArrayList<Node> children = new ArrayList<Node>();

    private Board board;
    private GameScreen gameScreen;


    /*
     ***********************************************************************
     *                              CONSTRUCTORS                           *
     ***********************************************************************/

    public Node() {

    }
    public Node(Board board) {
        this.board = board;
    }

    public Node(Board board, GameScreen gameScreen) {
        this.board = board;
        this.gameScreen = gameScreen;
    }

    /*
     ***********************************************************************
     *                                 METHODS                             *
     ***********************************************************************/
    public int findValue() {
        int bestValue = 0;
        //If no clear winner, check the board state and then determine the best values
        //from there. I.e. use the inference system.

        //TODO: need to say which player won
        if (board.hasWon()) {
            if      ( gameScreen.isPlayerWinner()) return Integer.MIN_VALUE;
            else if (!gameScreen.isPlayerWinner()) return Integer.MAX_VALUE;
            else if ( board.draw())                return 0;
        }



        return bestValue;
    }


    //------------------------------------ GETTERS & SETTERS
    public NodeType getNode() {
        return nodeType;
    }

    public void setNode(NodeType nodeType) {
        this.nodeType = nodeType;
    }

    public NodeType getType() {
        return nodeType;
    }

    public int getNodeID() {
        return columnID;
    }

    public void setNodeID(int columnID) {
        this.columnID = columnID;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getBestValue() {
        return bestValue;
    }

    public void setBestValue(int bestValue) {
        this.bestValue = bestValue;
    }


    //--------- Array methods
    public void addChild(Node child) {
        children.add(child);
    }

    public int getChildrenAmount() {
        return children.size();
    }

    public void findChildren() {
        board.generateMoves(children);
    }

    public Node getChildNode(int index) {
        return children.get(index);
    }

    public Node getAllChildren() {
        for (Node n : children) {
            return n;
        }

        return null;
    }



}// End class
