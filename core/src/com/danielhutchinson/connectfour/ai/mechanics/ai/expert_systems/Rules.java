package com.danielhutchinson.connectfour.ai.mechanics.ai.expert_systems;

/**
 * Created by Daniel on 18/12/2015.
 *
 *
 */
public class Rules {

    boolean matched;
    int     weight;

    String antecedant1, antecedant2, consequent;

    public Rules() {
        matched = false;
        weight = 0;
    }

    public void setRule( String antecedant1, String antecedant2, String consequent) {
        this.antecedant1 = antecedant1;
        this.antecedant2 = antecedant2;
        this.consequent  = consequent;
    }

}// End class
