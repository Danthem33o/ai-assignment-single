package com.danielhutchinson.connectfour.ai.mechanics.ai.expert_systems;

import com.danielhutchinson.connectfour.ai.board.Board;
import com.danielhutchinson.connectfour.ai.enums.CounterType;
import com.danielhutchinson.connectfour.ai.screens.GameScreen;

/**
 * Created by Daniel on 18/12/2015.
 *
 * Inference engine assigns the heuristic value of each move.
 * These values are then used in the minimax algorithm to determine the
 * best move to make.
 *
 * Each rule classifies a threat and gives a possible solution to them.
 *
 * These values will be determined from game strategies.
 *
 * Also, a move that results in an immediate win will return the value of
 * positive infinity, and negative infinity for the opposing player.
 *
 * STRATEGY:
 * store all possible game positions in DB, explore tree of game play from
 * each position, and determine the winner in each case.
 */
public class InferenceEngine {

    Board board;
    GameScreen gameScreen;

    boolean goodMove;

    public void InferenceEngine(Board board, GameScreen gameScreen) {
        this.board = board;
        this.gameScreen = gameScreen;
    }

    /*
    first player ALWAYS wins if takes middle column on first turn.
    if not, the second player can force a draw.
     */

    /*
    1) if winning move, take move
    2) if oponnent has winning move, take the move
    3) take center square over edges and corner
    4) take edges if only thing available
     */

    public void checkBoardAndMove() {
        //Satisfies criteria 1 & 2
        //if (there is a winnable move) board.placeCounter(1);
        //if (no other best move) board.placeCounter(edges);

        //values of corners are assigned low.

    }

    /*
    break rules into two classes:
    rules guaranteeing certain results (require proof)
    heuristic rules that are generally advantageous
     */

    /*
    useless threat is one that can't be carried out.

    threat == square that results in game winning four.
     */

    /**
     *  Traps in connect four:
     *  Either disrupt the opponent or create traps
     *  Trap consists of 3 checkers, either diagonally or horizontally.
     */
    //If missing checker in trap is in lower position than the missing
    //checker in the traps the opponent could build, build trap.
    //Else disrupt opponent.
    public void trap() {

    }

    //When no obvious move is found, mirror the opponent
    public void dissrupt() {

        Board board = new Board();

        //Cycle through the array and then do shit
        for (int row = 0; row < 6; row++) {
            //int lastMove = board.getLastMove();
            //board.placeCounter(lastMove);
        }

    }

    public void checkMove() {
        if (!goodMove) {
            dissrupt();
        } else {
            placeMove();
        }
    }

    public void placeMove(){

    }

    public void checkIfWinner(int column, int row, CounterType counterType) {
        boolean pTurn = gameScreen.isPlayersTurn();
        CounterType ct = pTurn ? CounterType.PLAYER : CounterType.AI;

        boolean gameOver = board.gameOver(column, row, counterType);
        if (gameOver) {
            /*if (ct) {

            }*/
        }
    }

    /**
     * If two perfect players, the only way to win is to go first and
     * play in the centre column
     */

    /**
     * Best place to put the chips is down the  middle of the board
     */

    /**
     * Will lose if the chip is placed in the four outermost columns
     * providede the opponent plays a perfect game
     */

    /**
     * If placing the chips in the two inner columns, the opponents will
     * draw
     */


    /**
     * quickest win is in 7 moves.
     */


    /*******************
     *
     * Rule 1: If having a winning move, take it.
     * Rule 2: If the opponient has a winning move, block it.
     * Rule 3: If I can create a fork (two winning ways) afer this move, do it.
     * Rule 4: Do not let the opponent creating a fork after my move. (Opponent may block your winning move and create a fork).
     * Rule 5: Place in the position such as I may win in thr most number of possible ways.
     *
     * Rule 5, need to count the number of possible winning ways.
     *
     */




    //Win loss states given where the first move is made.
    //If user plays down the middle, then can force a draw
    //Player 1 must make the first move to win.
    //PLayer 2 can make defensive moves to force a tie.
    //If player 1 makes move anywhere than the centre then player 2 must take center
    //If player 1 makes move in center, then player 2 must make move next to center to
    //force a draw.
    //If player 1 takes middle, then player two can't take outer columns, else loss.
    /**
     *     o o o o o o o
     *     o o o o o o o
     *     o o o o o o o
     *     o o o o o o o
     *     o o o o o o o
     *     o o o o o o o
     *     | | | | | | |
     *     x x D W D x x
     */

}// End class
