package com.danielhutchinson.connectfour.ai.board;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.danielhutchinson.connectfour.ai.enums.CounterType;

/**
 * Created by Daniel on 16/12/2015.
 *
 * Sets the counter type and gets the counter type from the
 * enum class.
 */
public class Counter extends Image {

    CounterType type = CounterType.EMPTY;

    public Counter() {

    }

    public CounterType getType() {
        return type;
    }

    public void setType(CounterType type) {
        this.type = type;
    }
}
