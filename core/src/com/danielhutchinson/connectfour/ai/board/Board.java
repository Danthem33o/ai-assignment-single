package com.danielhutchinson.connectfour.ai.board;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.danielhutchinson.connectfour.ai.enums.CounterType;
import com.danielhutchinson.connectfour.ai.mechanics.ai.game_tree.Node;
import com.danielhutchinson.connectfour.ai.screens.GameScreen;
import com.danielhutchinson.connectfour.ai.utils.Constants;
import com.danielhutchinson.connectfour.ai.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Daniel on 15/12/2015.
 *
 * Creates the board. Uses a multi-dimensional array of Counters to hold the chips in play.
 * The image of the board and counters are drawn in after.
 *
 * Holds all of the methods to add to the array, take away and check if won.
 */
public class Board extends Table {

    /*
     *************************************************************************************
     *                                      ATTRIBUTES                                   *
     *************************************************************************************/

    /* Width & height properties of the board */
    public static final int WIDTH = 7, HEIGHT =6;

    /* The board, where the counters are placed */
    Counter[][] counters = new Counter[WIDTH][HEIGHT];
    int lastMove;

    GameScreen gameScreen;



    /*
     **************************************************************************************
     *                                      CONSTRUCTORS                                  *
     **************************************************************************************/

    /**
     * Sets the game screen to the one passed in its parameters.
     *
     * It also sets the size of the whole board by using the
     * constants BOARD_WIDTH and BOARD_HEIGHT located in the constants
     * class, and then the size of each cell in the table by using
     * the constants WIDTH and HEIGHT located within this class.
     *
     * @param gameScreen  needed for methods called from gameScreen class
     */
    public Board(GameScreen gameScreen) {

        this.gameScreen = gameScreen;
        debug();
        setSize(Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT);
        defaults().size(getWidth() / WIDTH, getHeight() / HEIGHT);
        setup();

    }

    public Board() {
    } //Empty param



    /*
     **************************************************************************************
     *                                         METHODS                                    *
     **************************************************************************************/

    //---------------------------------------- board methods
    /**
     * setup() creates the table, which represents the connect four
     * board.
     *
     * Board dimensions are the standard connect four, 7x6.
     *
     * These dimensions can be passed as a parameter for a potential
     * game improvement.
     * Or use a scroller to make the board larger or smaller with
     * minimum height and widths. This enables a fair board.
     *
     * The first for loop (i), adds all of the rows, i.e. height (6).
     * The second (j), adds all of the columns, i.e width(7).
     */
    public void setup() {

        //Cycle through board and set as blank
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++){
                counters[j][i] = new Counter(); //Add a blank counter to each cell of the array
                add(counters[j][i]); //Add
                final int column = j;
                counters[j][i].addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        super.clicked(event, x, y);
                        gameScreen.takePlayersTurn(column);
                    }
                });
            }
            row();
        }

    }

    /**
     * Fills the cell colour of column specified with the
     * user's colour.
     * @param column column the user chose to add to
     * @param pTurn  finds which user added the counter
     */
    public void fillCell(int column, boolean pTurn) {

        //loop through each row in the column given
        for (int i = HEIGHT - 1; i >= 0; i--) {
            if (counters[column][i].getType() != CounterType.EMPTY) {
                //Add the counter image & colour the correct user counter...
                getCell(counters[column][HEIGHT - 1 - i]).getActor().setDrawable(new TextureRegionDrawable(new TextureRegion(new Texture("img/counter_white.png"))));
                getCell(counters[column][HEIGHT - 1 - i]).getActor().setColor(pTurn ? Color.RED : Color.YELLOW);
                return;
            }
        }

    }

    /**
     * Resets the board, both the array and drawable image.
     * Also sets the player turn to the original turn.
     */
    public void resetBoard() {

        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++){
                counters[i][j].setType(CounterType.EMPTY);
                counters[i][j].setDrawable(null);
            }
        }

        gameScreen.setPlayersTurn(true);

    }

    public void generateMoves(ArrayList<Node> moves) {
        /* amount of nodes to create */
        for (int column = 0; column < WIDTH; column ++) {
            if (hasSpace()) {
                Node node = new Node(this);
                node.setNodeID(column);
                moves.add(node);
            }
        }

    }

    public void generateIntMoves(ArrayList<Integer> moves) {
        /* amount of move to create */
        /*for (int column = 0; column < WIDTH; column ++) {
            for (int row = 0; row < HEIGHT; row ++)
                if (hasSpace()) {
                    moves.add(column);
                }
        }*/

        for (int column = 0; column < WIDTH; column ++) {
            if (isValidMove(column)) {
                moves.add(column);
            }
        }
    }


    //------------------------------------------ counter methods
    /**
     * Adds a counter to a column specified by the user.
     * Adds to the counter array only. No animation is ran.
     *
     * Checks each row to see if empty, if empty it adds.
     *
     * If column is full, its returns false.
     *
     * @param column the column which the counter is being added to
     * @return boolean true or false
     */
    public boolean placeCounter(int column) {
        //find out who's turn it is.
        boolean pTurn = gameScreen.isPlayersTurn();

        for (int i = 0; i < counters[column].length; i++) {
            //If row is empty add piece to the board...
            if (counters[column][i].getType() == CounterType.EMPTY) {

                counters[column][i].setType(pTurn ? CounterType.PLAYER : CounterType.AI);
                fillCell(column, pTurn);

                //add last move to table
                lastMove = i;

                printTable(); //testing purposes

                CounterType ct = pTurn ? CounterType.PLAYER : CounterType.AI;
                if (gameOver(column, i, ct)) {
                    //Send alert won, then reset after user has pressed ok
                    gameScreen.setWon(true);
                    if(pTurn){
                        gameScreen.setIsPlayerWinner(true);
                        System.out.println("Player won");
                    } else {
                        System.out.println("AI won");
                    }
                    resetBoard();

                    return true;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Finds a random column (move) to place the counter into.
     *
     * @return randomColumn
     */
    public int makeRandomMove() {
        return Utils.randomValueBetween(6, 0);
    }

    public boolean addCounterToArray(int column) {
        boolean pTurn = gameScreen.isPlayersTurn();

        for (int i = 0; i < counters[column].length; i++) {
            if (counters[column][i].getType() == CounterType.EMPTY) {

                counters[column][i].setType(pTurn ? CounterType.PLAYER : CounterType.AI);

                lastMove = i;

                printTable();

                if (hasWon()) {
                    gameScreen.setWon(true);
                    if (pTurn){
                        gameScreen.setIsPlayerWinner(true);
                    } else gameScreen.setIsPlayerWinner(false);
                }

                if (gameScreen.isPlayersTurn())   gameScreen.setPlayersTurn(false);
                else                              gameScreen.setPlayersTurn(true);

                return true;
            } else {

            }
        }

        return false;
    }

    public void fillTable() {
        boolean pTurn = gameScreen.isPlayersTurn();

        for (int i = 0; i < WIDTH; i++) {
            for (int j = HEIGHT - 1; j >= 0; j--) {
                if (counters[i][j].getType() != CounterType.EMPTY) {
                    getCell(counters[i][HEIGHT - 1 - i]).getActor().setDrawable(new TextureRegionDrawable(new TextureRegion(new Texture("img/counter_white.png"))));
                    getCell(counters[i][HEIGHT - 1 - i]).getActor().setColor(pTurn ? Color.RED : Color.YELLOW);
                    return;
                }
            }
        }

    }

    public void makeTurn(int column) {
        addCounterToArray(column);
        fillTable();

        boolean pTurn = gameScreen.isPlayersTurn();

    }

    public Board duplicate(Board board) {


        return board;
    }

    //------------------------------------------- boolean methods

    /**
     * Checks to see if a player has won at the end of the
     * placeCounter() method.
     *
     * This is always checked every time a counter is placed.
     *
     * It adds 1 to the total, and if total is >= 4, then the
     * for loop is exited and it returns true, else it returns
     * false at the end of the method.
     *
     * @param column   used to check if on board
     * @param row      used to check if on board
     * @param type     checks to see if user, AI or blank.
     * @return boolean true or false
     */
    public boolean gameOver(int column, int row, CounterType type){

        int total = 0;

        //D-right
        //-3 check farthest counter, move inwards
        for (int i = -3; i < 4; i++) { //i is counter location
            //Check if i & j is on board.
            //If not on board, check next counter

            //Diagonal only-right
            if (onBoard(column + i, row + i)) {
                //if tile is == to the player's counter, add one to total, else reset total
                total = counters[column + i][row + i].getType() == type ? total + 1 : 0;
                if (total >= 4) {
                    //if type is max, set positive infinity else set negative infinity
                    return true;
                }
            }
        }
        total = 0;

        //--- Repeat for all other directions...

        //left to right
        for ( int i = -3; i < 4; i++){
            if (onBoard(column + i, row)) {
                total = counters[column + i][row].getType() == type ? total + 1 : 0;
                if (total >= 4) return true;
            }
        }
        total = 0;

        //down to up
        for (int i = -3; i < 4; i++) {
            if (onBoard(column, row + i)) {
                total = counters[column][row + i].getType() == type ? total + 1 : 0;
                if (total >= 4)return true;
            }
        }
        total = 0;

        //D-left
        for (int i = -3; i < 4; i++) {
            //Check if i & j is on board.
            //If not on board, check next counter

            //Diagonal-left
            if (onBoard(column + i, row - i)) {
                total = counters[column + i][row - i].getType() == type ? total + 1 : 0;
                if (total >= 4) return true;
            }

        }

        return false;
    }

    public boolean draw() {
        /* cycle through the board and check if each cell has a p value */
        for (int column = 0; column < WIDTH; column ++) {
            for (int row = 0; row < HEIGHT; row ++) {
                if (counters[column][row].getType() == CounterType.EMPTY) {
                    gameScreen.setDraw(true);
                    resetBoard();
                    System.out.println("Game is a draw.");
                    return false;
                }
            }
        }
        return true;
    }

    public boolean hasWon() {
        int total = 0;
        boolean pTurn = gameScreen.isPlayersTurn();
        CounterType type = pTurn ? CounterType.PLAYER : CounterType.AI;

        //Diagonal
        for (int column = 0; column < WIDTH; column++) { // left
            for (int row = 0; row < HEIGHT; row ++) {
                for (int i = -3; i < 4; i ++) {
                    if (onBoard(column + i, row -i)) {
                        total = counters[column + i][row - i].getType() == type ? total + 1 : 0;
                        if (total >= 4) {
                            if (type == CounterType.PLAYER) gameScreen.setIsPlayerWinner(true);
                            else gameScreen.setIsPlayerWinner(false);
                            return true;
                        }
                    }
                }
            }
        }
        total = 0;

        for (int column = 0; column < WIDTH; column++ ) {//right
            for (int row = 0; row < HEIGHT; row ++) {
                for (int i = - 3; i < 4; i++) {
                    if (onBoard(column + i, row + i)) {
                        total = counters[column + i][row + i].getType() == type ? total + 1 : 0;
                        if (total >= 4) {
                            if (type == CounterType.PLAYER) gameScreen.setIsPlayerWinner(true);
                            else gameScreen.setIsPlayerWinner(false);
                            return true;
                        }
                    }
                }
            }
        }
        total = 0;

        //Horizontal
        for (int column = 0; column < WIDTH; column++) {
            for (int row = 0; row < HEIGHT; row ++) {
                for (int i = -3; i < 4; i++) {
                    if (onBoard(column + i, row)) {
                        total = counters[column + i][row].getType() == type ? total + 1 : 0;
                        if (total > 4) {
                            if (type == CounterType.PLAYER) gameScreen.setIsPlayerWinner(true);
                            else gameScreen.setIsPlayerWinner(false);
                            return true;
                        }
                    }
                }
            }
        }
        total = 0;

        //Verticle
        for (int column = 0; column < WIDTH; column++) {
            for (int row = 0; row < HEIGHT; row++) {
                for (int i = -3; i < 4; i++) {
                    if (onBoard(column, row + i)) {
                        total = counters[column][row + i].getType() == type ? total + 1 : 0;
                        if (total > 4) {
                            if (type == CounterType.PLAYER) gameScreen.setIsPlayerWinner(true);
                            else gameScreen.setIsPlayerWinner(false);
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks to see if the counter is on the board.
     *
     * @param column used to check if on the board.
     * @param row    used to check if on the board.
     * @return       true or false
     */
    public boolean onBoard(int column, int row) {

        return column >= 0 && column < WIDTH && row >= 0 && row < HEIGHT;

    }

    /**
     * Checks the column of the board specified to see if all
     * the available spaces are taken.
     *
     * @param column  The column to be checked.
     * @return        true or false, true if the column has an empty slot,
     *                else false;
     */
    public boolean isValidMove(int column) {
        //Cycle through column and check if its empty
        for (int j = 0; j < HEIGHT; j ++) {
            //Check if empty in index location
            if (counters[column][j].getType() == CounterType.EMPTY)
                return true;
        }

        return false;
    }

    /* check to see if there is space on the board in any column */
    public boolean hasSpace() {
        for (int column = 0; column < WIDTH; column ++) {
            for (int row = 0; row < HEIGHT; row ++) {
                if (counters[column][row].getType() == CounterType.EMPTY) return true;
            }
        }
        return false;
    }

    //may need function to get the board state

    public void undoMove(int column) {
        counters[column][lastMove].setType(CounterType.EMPTY);
        System.out.println("Removed from: " + column + " " + lastMove);
    }


    //------------------------------------- TEST METHODS
    /**
     * Test method to show that the counters have shown up in the
     * correct position on the libGDX table.
     *
     * It prints tbe value in each cell of tbe multi-dimensional
     * array of Counters.
     */
    private void printTable() {

        for (int i = HEIGHT - 1; i >= 0; i--) {
            for (int j = 0; j < WIDTH; j++){
                System.out.print(counters[j][i].getType() != CounterType.EMPTY ? (counters[j][i].getType() == CounterType.PLAYER ? "P " : "C ") : "- ");
            }
            System.out.println();
        }

        System.out.println();

    }

}// End class
