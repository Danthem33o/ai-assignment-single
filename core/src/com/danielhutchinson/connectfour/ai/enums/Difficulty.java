package com.danielhutchinson.connectfour.ai.enums;

/**
 * Created by Daniel on 14/12/2015.
 */
public enum Difficulty {

    EASY,
    MEDIUM,
    HARD,
    EXTREME,
    IMPOSSIBLE

}// End enum
