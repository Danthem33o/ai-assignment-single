package com.danielhutchinson.connectfour.ai.enums;

/**
 * Created by Daniel on 15/12/2015.
 */
public enum CounterType {

    EMPTY,
    PLAYER,
    AI

}//End enum
