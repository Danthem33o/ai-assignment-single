package com.danielhutchinson.connectfour.ai.enums;

/**
 * Created by Daniel on 21/12/2015.
 */
public enum NodeType {

    UNKNOWN,
    TERMINAL,
    CHILD,
    PARENT

}// End enum
