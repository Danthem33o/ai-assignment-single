package com.danielhutchinson.connectfour.ai;

import com.badlogic.gdx.Game;
import com.danielhutchinson.connectfour.ai.enums.Difficulty;
import com.danielhutchinson.connectfour.ai.screens.SplashScreen;
import com.danielhutchinson.connectfour.ai.utils.Assets;

/**
 * Core game class.
 *
 * Holds the title, version No. and the difficulty.
 * This is the 'main' class and is the first thing fired when the
 * program runs.
 */
public class ConnectFour extends Game {

	public final static String TITLE = "AI Project", VERSION = "2.0";
	private Difficulty difficulty = Difficulty.EASY;
	
	@Override
	public void create () {
		//Set the skin and call the first screen to be loaded.
		Assets.create();
		setScreen( new SplashScreen(this));

	}

	@Override
	public void render () {
		super.render();
	}

	public void dispose() {
		super.dispose();
	}

	public void resize(int width, int height) {
		super.resize(width, height);
	}


	/*
	 ********************************************************************************
	 * 						       	  GETTERS & SETTERS		       				    *
	 ********************************************************************************/

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	/**
	 * Gets the next difficulty in the enum class starting
	 * from easy, then repeating itself when it gets to the
	 * final value: impossible.
	 */
	public void nextDifficulty(){
		difficulty = Difficulty.values()[(difficulty.ordinal() + 1) % Difficulty.values().length];
	}

}// End class
