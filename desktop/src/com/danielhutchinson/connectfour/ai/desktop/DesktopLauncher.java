package com.danielhutchinson.connectfour.ai.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.danielhutchinson.connectfour.ai.ConnectFour;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = ConnectFour.TITLE + "   v" + ConnectFour.VERSION;
		config.vSyncEnabled = true;
		config.width  = 960;
		config.height = 540;

		new LwjglApplication(new ConnectFour(), config);
	}
}// End class
